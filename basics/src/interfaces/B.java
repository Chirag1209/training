package interfaces;

public class B implements A{

	@Override
	public void method1() {
		System.out.println("interface method1");
		
	}

	@Override
	public void method2() {
		System.out.println("interface method2");
		
		
	}
	void thanks() {
		System.out.println("my own method");
	}
	
	public static void main(String[] args) {
		B obj = new B();
		obj.method1();
		obj.method2();
		obj.thanks();
	}

}
