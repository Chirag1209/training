package thisSuper;

class Myparent{
	String name="this is parent class";
}
public class Myclass extends Myparent {
	String name="this is instance variable";

	void somemethod() {
		String name= "this is local varible ...because i am inside method ";
		System.out.println(name);
		System.out.println(this.name);
		System.out.println(super.name);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Myclass mc = new Myclass();
        mc.somemethod();

	}

}
