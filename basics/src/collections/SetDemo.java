package collections;

import java.util.HashSet;
import java.util.Set;

public class SetDemo {
	
	public static void main(String[] args) {
		Set<String> sports= new HashSet<>();
		sports.add("tennis");
		sports.add("Base Ball");
		sports.add("kho-kho");
		sports.add("kabaddi");
		System.out.println(sports);
		sports.add("kho-kho");
		System.out.println(sports);
		int ts= sports.size();
		System.out.println(ts);
	}

}
