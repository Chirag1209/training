package staticdemo;

public class StaticBlock {
	static
	{
		System.out.println("checking with main method");
	}
	
	public static void main(String[] args) {
		System.out.println("this main method");
	}
}
