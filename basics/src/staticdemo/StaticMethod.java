package staticdemo;

public class StaticMethod {
	
	void play() {
		System.out.println("I play guitar");
	}
	static void sleep() {
		System.out.println("I sleep at 4");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sleep();
		StaticMethod sm= new StaticMethod();
		sm.play();

	}

}
